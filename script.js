function createNewUser() {
    // Запитати ім'я від користувача
    const firstName = prompt("Введіть ваше ім'я:");
  
    // Запитати прізвище від користувача
    const lastName = prompt("Введіть ваше прізвище:");
  
    // Запитати дату народження від користувача
    const birthdayInput = prompt("Введіть вашу дату народження (формат: dd.mm.yyyy):");
  
    // Розбити введений рядок з датою народження на день, місяць і рік
    const parts = birthdayInput.split(".");
    const day = parseInt(parts[0]);
    const month = parseInt(parts[1]) - 1; // Місяці в Date починаються з 0 (січень - 0, лютий - 1 і т.д.)
    const year = parseInt(parts[2]);
  
    // Перевірити, чи введена дата є коректною
    if (isNaN(day) || isNaN(month) || isNaN(year)) {
      console.log("Введено некоректну дату народження.");
      return null;
    }
  
    // Створити об'єкт Date з отриманими значеннями
    const birthday = new Date(year, month, day);
  
    // Перевірити, чи дата народження є дійсною
    if (isNaN(birthday.getTime())) {
      console.log("Введено некоректну дату народження.");
      return null;
    }
  
    // Функція для підрахунку віку з використанням дати народження
    function calculateAge() {
      const now = new Date();
      let age = now.getFullYear() - birthday.getFullYear();
      const monthDiff = now.getMonth() - birthday.getMonth();
      if (monthDiff < 0 || (monthDiff === 0 && now.getDate() < birthday.getDate())) {
        age--;
      }
      return age;
    }
  
    // Функція для отримання пароля з використанням ім'я, прізвища та року народження
    function generatePassword() {
      return firstName[0].toUpperCase() + lastName.toLowerCase() + year;
    }
  
    // Створити об'єкт newUser з властивостями firstName, lastName та birthday
    const newUser = {
      firstName: firstName,
      lastName: lastName,
      birthday: birthday,
      getAge: calculateAge,
      getPassword: generatePassword
    };
  
      // Повернути об'єкт newUser
  return newUser;
}

// Створити юзера за допомогою функції createNewUser()
const user = createNewUser();

if (user) {
  // Вивести в консоль результати роботи функцій createNewUser(), getAge() та getPassword() для створеного юзера
  console.log("Ім'я:", user.firstName);
  console.log("Прізвище:", user.lastName);
  console.log("Дата народження:", user.birthday.toLocaleDateString());
  console.log("Вік:", user.getAge());
  console.log("Пароль:", user.getPassword());
}